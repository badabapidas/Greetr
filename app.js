var arr = [
    1,
    false,
    {
        name: "Bapi",
        sex: "Male"
    },
    function (name) {
        var greeting = "hello ";
        // console.log(greeting + name)
    },
    "hello"
];
// console.log(arr);
arr[3](arr[2].name);

//IIFE
(function (name) {
    // console.log("Hello! "+name);
})('Bapi');

//Closure
(function greet(whattosay) {
    return function (name) {
        // console.log(whattosay + " " + name);
    }
})('Hey!')('Bapi');

//Closure Advance
function buildFunctions() {
    var arr = [];
    for (var index = 0; index < 3; index++) {
        // let j=index; es6
        // arr.push(function () {
        //     console.log(j);
        // });

        //older browser
        arr.push((function (j) {
            return function () {
                // console.log(j);
            }
        })(index));
    }
    return arr;
}

var fs = buildFunctions();
fs[0]();
fs[1]();
fs[2]();


// function factories
function makeGreeting(language) {
    return function (firstname, lastname) {
        if (language === 'en') {
            // console.log("Hello! " + firstname + ' ' + lastname);
        }
        if (language === 'es') {
            // console.log("Hola! " + firstname + ' ' + lastname);
        }
    }

}
makeGreeting('en')('Bapi', 'Das');
makeGreeting('es')('Bapi', 'Das');


//Callback
(function callBackExecute(callback) {
    // setTimeout(callback, 2000);
})(function () {
    // console.log('callback done');
});

// bind, apply, call
var person = {
    firstname: 'Bapi',
    lastname: 'Das',
    getFullName: function () {
        var fullname = this.firstname + ' ' + this.lastname;
        return fullname;
    }
}

var logName = function (param1, param2) {
    console.log('logged: ' + this.getFullName());
    console.log('Arguments: ' + param1 + ' ' + param2);
}
// bind creates a copy of the func logName
var logPersonName = logName.bind(person);
// logPersonName('param1');

// call method calling the func with the ref objects and params without create a copy of that
// logName.call(person, 'param1', 'param2');

// apply works the same was as call(), but only diff is its expects its arguments as an array format
// logName.apply(person, ['param1', 'param2']);


//function borrowing
var person2 = {
    firstname: 'John',
    lastname: 'Doe'
}
// console.log(person.getFullName.apply(person2));

//function currying
function multiply(a, b) {
    console.log(a * b);
}
// var multiplyBy2 = multiply.bind(this, 2)(4);

//functional programming
function mapForEach(array, fn) {
    var newArray = [];
    for (var index = 0; index < array.length; index++) {
        newArray.push(fn(arr[index]));
    }
    return newArray;
}
var arr = [1, 2, 3];
// console.log(arr);

var arr1 = mapForEach(arr, function (item) {
    return item * 2;
});
// console.log(arr1);

var arr2 = mapForEach(arr, function (item) {
    return item > 2;
});
// console.log(arr2);

var checkPastLimit = function (limiter, item) {
    return item > limiter;
}
// console.log(mapForEach(arr, checkPastLimit.bind(this, 1)));

var checkPastLimitSimplified1 = function (limiter) {
    return function (limiter, item) {
        return item > limiter;
    }.bind(this, limiter);
}
// console.log(mapForEach(arr, checkPastLimitSimplified1(1)));

var checkPastLimitSimplified2 = function (limiter) {
    return checkPastLimit.bind(this, limiter);
}
// console.log(mapForEach(arr, checkPastLimitSimplified2(1)));

//Prototype
function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
}

// advantage of adding methods in prototype is it will have a single copy; if u add this method in person object, it will create as many copy
// as many object creates
Person.prototype.getFullName = function () {
    return this.firstname + ' ' + this.lastname;
}
var bapi = new Person('Bapi', 'Das');
// console.log(bapi);
Person.prototype.getFormalName = function () {
    return this.lastname + ',' + this.firstname;
}

//Polyfill
if (!Object.create) {
    Object.create = function (o) {
        if (arguments.length > 1)
        { throw new Error('accepts only one arguments'); }

        function F() { }
        F.prototype = o;
        return new F();
    }; 
}


















